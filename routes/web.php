<?php

use App\Jobs\sendEmailJob;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/mail', function(){
    // Mail::to('ugkad31@gmail.com')->send(new SendEmailMailable());
    $job = (new sendEmailJob());
            // ->delay(Carbon::now()->addSeconds(5));
    
    dispatch($job);

    return "Email is sent";
});